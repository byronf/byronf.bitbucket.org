/**
*  Module
*
* Description
*/
var myApp = angular.module('myApp', []);

myApp.controller('MyController',  function MyController($scope) {

	$scope.author = {
		'name':'Byron Ferguson',
		'title':'Designer',
		'company':'Redmoon Consulting'
	}
});